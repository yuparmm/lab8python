# TASK1
# class Bank:
#     users = []
#     def __init__(self, name, password, balance=0):
#         self.name = name
#         self.password = password
#         self.balance = balance
#         Bank.users.append(self)
#
#     def add_balance(self, input_balance):
#         self.balance += input_balance
#         return self.balance
#
#     def remove_balance(self, input_balance):
#         if self.balance >= input_balance:
#             self.balance -= input_balance
#             return True
#         else:
#             return False
# def main():
#     try:
#         while True:
#             choose = int(input("Меню: \n1-Створити користувача \n2-Увійти \n0-Вийти \nВибір: "))
#             if choose == 1:
#                 new_name = input("Ім'я користувача: ")
#                 new_password = input("Новий пароль: ")
#                 if len(new_password) > 0 and len(new_name) > 0:
#                     Bank(new_name, new_password)
#                 else:
#                     print("Введені не всі дані!")
#             elif choose == 2:
#                 input_name = input("Введіть ім'я користувача: ")
#                 user_exists = False
#                 for user in Bank.users:
#                     if user.name == input_name:
#                         user_exists = True
#                         input_password = input("Введіть пароль: ")
#                         if user.password == input_password:
#                             choose_operation = int(input("Меню: \n1-Внести гроші \n2-Зняти гроші \n0-Вийти \nВибір: "))
#                             if choose_operation == 1:
#                                 input_balance = int(input("Введіть суму: "))
#                                 print(f"Баланс рахунку: {user.add_balance(input_balance)} грн.")
#                             elif choose_operation == 2:
#                                 input_balance = int(input("Введіть суму: "))
#                                 if user.remove_balance(input_balance):
#                                     print(f"Баланс рахунку: {user.balance} грн.")
#                                 else:
#                                     print("На рахунку недостатньо коштів.")
#                             elif choose_operation == 0:
#                                 break
#                             else:
#                                 print("Операція неправильна.")
#                         else:
#                             print("Пароль неправильний.")
#                         break
#                 if not user_exists:
#                     print("Користувача не знайдено.")
#             elif choose == 0:
#                 break
#             else:
#                 print("Операція неправильна.")
#     except ValueError:
#         print("Введено невірні дані.")
#
# main()



# TASK2
# import random
#
# class Coin:
#     def __init__(self):
#         self.__sideup = 'heads'
#
#     def toss(self):
#         if random.randint(0, 1) == 0:
#             self.__sideup = 'heads'
#         else:
#             self.__sideup = 'tails'
#
#     def get_sideup(self):
#         return self.__sideup
#
# coin = Coin()
#
# n = int(input("Введіть кількість підкидань монети: "))
#
# print("Підкидання монети:")
# for i in range(n):
#     coin.toss()
#     print("Підкидання {}: {}".format(i+1, coin.get_sideup()))



# TASK3
# class Car:
#     def __init__(self, brand, model, year):
#         self.brand = brand
#         self.model = model
#         self.year = year
#         self.speed = 0
#
#     def accelerate(self):
#         self.speed += 5
#
#     def brake(self):
#         if self.speed >= 5:
#             self.speed -= 5
#         else:
#             self.speed = 0
#
#     def get_speed(self):
#         return self.speed
#
# my_car = Car("Skoda", "Octavia", 2003)
#
# for i in range(5):
#     my_car.accelerate()
#     print(f"Поточна швидкість після прискорення: {my_car.get_speed()}")
#
# for i in range(5):
#     my_car.brake()
#     print(f"Поточна швидкість після гальмування: {my_car.get_speed()}")



# TASK4
# class Dog:
#     mammal = True
#     nature = "dogggg"
#
#     def __init__(self, name, age, breed):
#         self.name = name
#         self.age = age
#         self.breed = breed
#
#     def voice(self):
#         return "gav!"
#
#     def display_info(self):
#         print(f"Name: {self.name}, Age: {self.age}")
#
#
# class Labrador(Dog):
#     breed = "Labrador"
#
#     def play(self):
#         return "I like play with ball!"
#
#
# class Bulldog(Dog):
#     breed = "Bulldog"
#
#     def sleep(self):
#         return "sleeping."
#
#
# class Pets:
#     def __init__(self):
#         self.dogs = []
#
#     def add_dog(self, dog):
#         self.dogs.append(dog)
#
#     def show_pets_info(self):
#         for dog in self.dogs:
#             print("Breed:", dog.breed)
#             print("Nature:", dog.nature)
#             dog.display_info()
#             print()
#
#
# dog1 = Labrador("Max", 3, "Labrador")
# dog2 = Bulldog("Rocky", 5, "Bulldog")
# dog3 = Dog("Buddy", 2, "Mops")
#
# pets = Pets()
# pets.add_dog(dog1)
# pets.add_dog(dog2)
# pets.add_dog(dog3)
#
# pets.show_pets_info()


# TASK5
# import random
#
# class Buffer:
#     def __init__(self):
#         self.buffer = []
#
#     def get_current_part(self):
#         return self.buffer
#
#     def add(self, *values):
#         self.buffer.extend(values)
#         while len(self.buffer) >= 5:
#             print(f"Поточний буфер: {self.get_current_part()}\nСума перших п'яти чисел: {sum(self.buffer[:5])}")
#             del self.buffer[:5]
#
# def main():
#     buffer = Buffer()
#     while True:
#         new_values = []
#         while random.choice([True, True, False]):
#             new_values.append(random.randint(1, 10))
#         print(f"Згенеровані числа: {new_values}")
#         buffer.add(*new_values)
#         print(f"Поточний буфер: {buffer.get_current_part()}")
#         try:
#             choice = int(input("Бажаєте додати ще числа? (1 - так, 0 - скасувати): "))
#             if choice == 0:
#                 break
#             elif choice == 1:
#                 continue
#             else:
#                 print("Введено неправильне значення.")
#                 break
#         except ValueError:
#             print("Введено неправильне значення.")
#
# if __name__ == "__main__":
#     main()






# TASK6
# class NameLengthError(ValueError):
#     def __init__(self, name):
#         super().__init__(f"Довжина імені '{name}' коротша ніж 10 символів.")
#
# def check_name_length(name):
#     if len(name) < 10:
#         raise NameLengthError(name)
#
# try:
#     name = input("Введіть ім'я: ")
#     check_name_length(name)
# except NameLengthError as e:
#     print(e)


# TASK7
# class DecimalToRoman:
#     def __init__(self):
#         self.roman_numerals = {
#             1: 'I', 4: 'IV', 5: 'V', 9: 'IX', 10: 'X',
#             40: 'XL', 50: 'L', 90: 'XC', 100: 'C',
#             400: 'CD', 500: 'D', 900: 'CM', 1000: 'M'
#         }
#
#     def convert_to_roman(self, num):
#         if num <= 0 or num >= 4000:
#             raise ValueError("Число повинно бути в межах від 1 до 3999")
#
#         result = ''
#         for value, numeral in sorted(self.roman_numerals.items(), reverse=True):
#             while num >= value:
#                 result += numeral
#                 num -= value
#         return result
#
# converter = DecimalToRoman()
# print(converter.convert_to_roman(354))
# class RomanToDecimal:
#     def __init__(self):
#         self.roman_numerals = {
#             'I': 1, 'V': 5, 'X': 10, 'L': 50,
#             'C': 100, 'D': 500, 'M': 1000
#         }
#
#     def convert_to_decimal(self, roman):
#         result = 0
#         prev_value = 0
#         for numeral in reversed(roman):
#             value = self.roman_numerals[numeral]
#             if value < prev_value:
#                 result -= value
#             else:
#                 result += value
#             prev_value = value
#         return result
#
# converter = RomanToDecimal()
# print(converter.convert_to_decimal('CCCLIV'))

# TASK8
# class Shop:
#     def __init__(self, shop_name, store_type):
#         self.shop_name = shop_name
#         self.store_type = store_type
#         self.number_of_units = 0
#
#     def describe_shop(self):
#         print(f"Магазин {self.shop_name} працює {self.store_type}.")
#
#     def open_shop(self):
#         print(f"Магазин {self.shop_name} відчинений.")
#
#     def set_number_of_units(self, number):
#         self.number_of_units = number
#
#     def increment_number_of_units(self, increment):
#         self.number_of_units += increment
#
# class Discount(Shop):
#     def __init__(self, shop_name, store_type):
#         super().__init__(shop_name, store_type)
#         self.discount_products = []
#
#     def get_discount_products(self):
#         print("Знижка:", self.discount_products)
#
# print("\nА")
# store = Shop("ДК продукт", "офлайн")
# print(f"Назва магазину: {store.shop_name}")
# print(f"Формат роботи: {store.store_type}")
# store.describe_shop()
# store.open_shop()
#
# print("\nБ")
# storeA = Shop("Ашан", "онлайн/офлайн")
# storeB = Shop("Полісся продукт", "офлайн")
# storeC = Shop("Манго", "офлайн")
# storeA.describe_shop()
# storeB.describe_shop()
# storeC.describe_shop()
#
# print("\nС")
# storeD = Shop("Аптека", "офлайн")
# print(f"Кількість товарів: {storeD.number_of_units}")
# storeD.number_of_units = 20
# print(f"Нова кількість товару: {storeD.number_of_units}")
#
# print("\nD")
# storeE = Shop("Реал-маркет", "офлайн")
# storeE.set_number_of_units(40)
# print(f"Нова кількість товарів: {storeE.number_of_units}")
# storeE.increment_number_of_units(30)
# print(f"Оновлена кількість: {storeE.number_of_units}")
#
# print("\nE")
# store_discount = Discount("Олді", "офлайн")
# store_discount.discount_products = ["Ламінат", "Фарба", "Цемент"]
# store_discount.get_discount_products()



# TASK9
from user import User
from admin import Admin


print("\nA")
user1 = User("Богдан", "Млинський", "bodya@gmail.com", "Yuparm",active=True)
user2 = User("Максим", "Максимов", "max@gmail.com", "Maxy", active=False)
user1.describe_user()
user1.greeting_user()
print("\n")
user2.describe_user()
user2.greeting_user()


print("\nB")
user1.increment_login_attempts()
user1.increment_login_attempts()
print(f"Кількість спроб входу: {user1.login_attempts}")
user1.reset_login_attempts()
print(f"Кількість спроб входу після скидання: {user2.login_attempts}")


print("\nC D")
admin = Admin("Адмін", "Адмінов", "adminchik@gmail.com", "BATYA")
admin.privileges.show_privileges()

def main():
    pass

if __name__ == "__main__":
    main()

















