from user import User

class Admin(User):
    def __init__(self, first_name, last_name, email, username, active=False):
        super().__init__(first_name, last_name, email, username, active)
        self.privileges = Privileges()

class Privileges:
    def __init__(self):
        self.privileges = ["Дозволено переглядати", "Дозволено редагувати", "Дозволено додавати"]

    def show_privileges(self):
        print("Права адміністратора:")
        for privilege in self.privileges:
            print(f"- {privilege}")