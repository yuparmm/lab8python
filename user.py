
class User:
    def __init__(self, first_name, last_name, email, username, active=False):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.username = username
        self.active = active
        self.login_attempts = 0

    def describe_user(self):
        print(f"Повне ім'я: {self.first_name} {self.last_name}")
        print(f"Електронна адреса: {self.email}")
        print(f"Нікнейм: {self.username}")
        if self.active:
            print("Активний користувач")
        else:
            print("Не активний користувач")

    def greeting_user(self):
        print(f"Привіт, {self.last_name} {self.first_name}!")

    def increment_login_attempts(self):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts = 0
